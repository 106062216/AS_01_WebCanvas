var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
ctx.strokeStyle = '#000000';
ctx.lineJoin = ctx.lineCap = 'round';


var mouse = (0, 0);
var startPos = (0, 0);
var lastPos = (0, 0);
var x = 0;
var y = 0;


function getMousePos(event) {
	return {
		x: event.clientX - canvas.offsetLeft,
		y: event.clientY - canvas.offsetTop
	};
}

var isDrawing;

canvas.onmousedown = function mouseDown(event) {
	isDrawing = true;
	mouse = getMousePos(event);
	startPos = getMousePos(event);
	ctx.beginPath();
	ctx.moveTo(mouse.x, mouse.y);
	lastPos = mouse;
};

canvas.onmousemove = function mouseMove(event) {
	if (isDrawing) {
		mouse = getMousePos(event);
		if (toolsNow === "paintBrush" || toolsNow === "eraser") {
			ctx.lineTo(mouse.x, mouse.y);
			ctx.stroke();
		}
		else if (toolsNow === "square") {
			ctx.putImageData(cPushArray[cStep], 0, 0);
			ctx.strokeRect(startPos.x, startPos.y, mouse.x - startPos.x, mouse.y - startPos.y);
		}
		else if (toolsNow === "circle") {
			ctx.putImageData(cPushArray[cStep], 0, 0);
			ctx.beginPath();
			var r = Math.sqrt((mouse.x - startPos.x)*(mouse.x - startPos.x) + (mouse.y - startPos.y)*(mouse.y - startPos.y));
			ctx.arc(startPos.x, startPos.y, r, 0, Math.PI * 2, 0);
			ctx.stroke();
			lastPos = mouse;
		}
		else if (toolsNow === "triangle") {
			ctx.putImageData(cPushArray[cStep], 0, 0);
			ctx.beginPath();
			ctx.moveTo(startPos.x, startPos.y);
			ctx.lineTo(mouse.x, mouse.y);
			ctx.lineTo(2*startPos.x - mouse.x, mouse.y);
			ctx.closePath();
			ctx.stroke();
			lastPos = mouse;
		}
	}

	if (txtMove) {
		moveText();
	}
}

canvas.onmouseup = function mouseUp(event) {
	isDrawing = false;
	cPush();
}

//change tools
var toolsNow;
var toolsLast = '';

function tools(category) {
	toolsNow = category;
	changeCursor();
	focus(category);

	if (toolsNow === "eraser") {
		ctx.globalCompositeOperation = "destination-out";
	} else {
		ctx.globalCompositeOperation = "source-over";
		ctx.strokeStyle = document.getElementById("colorPicker").style.color;
	}

	if (toolsNow !== "textblock") {
		txt.style.display = "none";
	}

}

function focus(category) {
	if (toolsLast === "circle" || toolsLast === "square" || toolsLast === "triangle") {
		document.getElementById('shape').style.color = '#FFFFFF';
	} else if (toolsLast !== ''){
			document.getElementById(toolsLast).style.color = '#FFFFFF';
	}
	toolsLast = category;

	if (toolsNow === "circle" || toolsNow === "square" || toolsNow === "triangle") {
		document.getElementById('shape').style.color = '#000000';
	}
	else {
		document.getElementById(toolsNow).style.color = '#000000';
	}

}

//change cursor
function changeCursor() {
	var cursor = document.getElementById("canvas-container");
	if (toolsNow === "eraser") {
		cursor.style.cursor = "url( 'https://img.icons8.com/material-rounded/24/000000/erase.png' ) 0 24, auto";
	} else if (toolsNow === "paintBrush") {
		cursor.style.cursor = "url( 'https://img.icons8.com/android/24/000000/paint.png' ) 0 24, auto";
	} else {
		cursor.style.cursor = "default";
	}
}

//upload img
document.getElementById('upImg').onchange = function () {
	var img = new Image();
	try {
		img.onload = function draw() {
			ctx.beginPath();
			ctx.drawImage(img, 0, 0, img.width, img.height);
			ctx.stroke();
			cPush();
			document.getElementById('upImg').value = null;
		}
	} catch (e) {
		console.log(e);
	}
	img.src = URL.createObjectURL(this.files[0]);
};

//choose color
var colorPicker = document.getElementById("colorPicker");

function Pick(color) {
	colorPicker.style.color = color;
	ctx.strokeStyle = color;
	ctx.fillStyle = color;
}

//stroke width by using slider
var slider = document.getElementById("myRange");
var brushSize = document.getElementById("BS");
brushSize.value = slider.value + "px";
ctx.lineWidth = Number(slider.value);

slider.oninput = function () {
	// Update the current slider value (each time you drag the slider handle)
	brushSize.value = this.value + "px";
	ctx.lineWidth = Number(slider.value);
}

//font and size
var fontNow = 'Arial';
var sizeNow = '24px';
ctx.font = sizeNow + ' ' + fontNow;

document.getElementById('inSize').onchange = function () {
	var tmp = this.value + 'px';
	sizePicker(tmp);
}

function sizePicker(size) {
	sizeNow = size;
	ctx.font = sizeNow + ' ' + fontNow;
}

function fontPicker(font) {
	document.getElementById("font").style.fontFamily = font;
	fontNow = font;
	ctx.font = sizeNow + ' ' + fontNow;
}

//textblock
var txtInner = document.getElementById("textbar");
var txt = document.getElementById("inTxt");
var txtMove = false;

function creatTextblock() {
	txt.style.display = "unset";
	txt.style.left = '350px';
	txt.style.top = '200px';
	txt.value = "text";
	tools("textblock");
}

txt.onclick = function clickText() {
	txtMove = !txtMove;
};

function moveText() {
	txt.style.left = String(event.clientX) + "px";
	txt.style.top = String(event.clientY) + "px";
}

txt.onkeypress = function enterText(event) {
	if (event.keyCode == 13) {
		var x = Number((txt.style.left).slice(0, -2));
		var y = Number((txt.style.top).slice(0, -2));
		ctx.fillText(txt.value, x - canvas.offsetLeft, y - canvas.offsetTop);
		txt.style.display = "none";
		txtMove = false;
		cPush();
	}
};

// push, undo, redo, clear
var cPushArray = new Array();
var cStep = 0;
cPushArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));

function cPush() {
	cStep++;
	if (cStep < cPushArray.length) {
		cPushArray.length = cStep;
	}
	cPushArray.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
}

function cUndo() {
	console.log(cStep);
	if (cStep > 0) {
		cStep--;
		ctx.putImageData(cPushArray[cStep], 0, 0);
	}
}

function cRedo() {
	console.log(cStep);
	if (cStep < cPushArray.length - 1) {
		cStep++;
		console.log(cPushArray[cStep]);
		ctx.putImageData(cPushArray[cStep], 0, 0);
	}
}

function Clear() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	cPush();
}

